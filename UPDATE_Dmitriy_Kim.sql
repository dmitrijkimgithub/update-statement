-- Alter the rental duration and rental rates of the film you inserted before to three weeks and 9.99, respectively.

UPDATE film 
SET rental_duration = 21,
rental_rate = 9.99
WHERE title = 'SPIDER-MAN. NO WAY HOME';

-- Alter any existing customer in the database with at least 10 rental and 10 payment records. Change their personal data to yours (first name, last name, address, etc.). 
-- You can use any existing address from the "address" table. Please do not perform any updates on the "address" table, as this can impact multiple records with the same address.

UPDATE customer 
SET first_name = 'DMITRIY',
last_name = 'KIM', address_id = 5, email = 'DMITRIJKIM243@gmail.com'
WHERE customer_id = 1; 
-- Please, if you didn't find the changed customer_id, choose order customer_id by asc

-- Change the customer's create_date value to current_date.

UPDATE customer 
SET create_date = current_date 
WHERE customer_id = 1